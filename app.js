var itemIndex = 0;

function GetFreeItemIndex() {
    itemIndex++;
    return itemIndex;
}

var testData = [];

if(localStorage.getItem('todo')!=undefined)
{testData=JSON.parse(localStorage.getItem('todo'));
}



var todoListApp = new Vue({

    el: '#app-todolist',

    data: {
        todoItems: testData,
        newTodoText: ""
    },

    methods: {

        addTodo: function() {

           var i=GetFreeItemIndex();
           var t=this.newTodoText;

            this.todoItems.push({Id:i, Text:t })

            this.newTodoText = "";
localStorage.setItem('todo',JSON.stringify(testData))


        },

        removeTodo: function(todo) {

            var ind=this.todoItems.indexOf(todo)

            this.todoItems.splice(ind,1);

            localStorage.setItem('todo',JSON.stringify(testData))
        }

    }
});